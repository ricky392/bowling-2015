note
	description: "Summary description for {GAME_IMPLEMENTATION}."
	author: "Mattia Monga"

class
	GAME_IMPLEMENTATION

inherit

	GAME

feature

	make
		do
			create rolls.make_filled (0, 0, 21)
		end

	score: INTEGER
		do
			across rolls as i
			loop
			total_score := total_score + i.item
			end
			Result := total_score
		end

	roll (pins: INTEGER)
		do
			rolls.put (pins, total_roll)
			total_roll := total_roll + 1
		end


	current_score: INTEGER

	total_roll: INTEGER

	total_score: INTEGER

	spare_check : BOOLEAN

	strike_check : BOOLEAN

	ended: BOOLEAN
		do
			if total_roll = 20 then
				Result := True
			end
		end

feature {NONE}

	current_roll: INTEGER

invariant
	valid_roll: 0 <= current_roll and current_roll <= 20

end
